#!/bin/sh

set -eu

[ $# -ge 2 ] || {
    echo "Usage: debian/setup-mysql.sh port data-dir" >&2
    exit 1
}

# CLI arguments #
port=$1
datadir=$2
action=${3:-start}
if [ "$(id -u)" -eq 0 ]; then
    user="mysql"
else
    user="$(whoami)"
fi

# Some vars #

socket=$datadir/mysql.sock
# Commands:
mysqladmin="mysqladmin --no-defaults --user root --port $port --host 127.0.0.1 --socket=$socket --no-beep"
mysqld="/usr/sbin/mysqld --no-defaults --user=$user --bind-address=127.0.0.1 --port=$port --socket=$socket --datadir=$datadir"

# Main code #

if [ "$action" = "stop" ]; then
    echo "Stopping MySQL..."
    if $mysqladmin ping 2>/dev/null; then
        pid=$(lsof -Fp -n -i TCP:$port -s TCP:LISTEN | sed -n -s 's/^p//p')
        if [ -z "$pid" ]; then
            echo "Unable to find MySQL port" >&2
            exit 1
        fi
        $mysqladmin shutdown || true
        for i in 1 2 3 4 5 6 7 8 9 10; do
            sleep 1
            if ! $mysqladmin ping 2>/dev/null; then
                exit
            fi
        done
        echo "Killing MySQL database server by signal" >&2
        kill -15 $pid
        for i in 1 2 3 4 5 6 7 8 9 10; do
            sleep 1
            if ! $mysqladmin ping 2>/dev/null; then
                exit
            fi
        done
        kill -9 $pid
    fi
    exit
fi

rm -rf $datadir
mkdir -p $datadir
chmod go-rx $datadir
chown $user: $datadir

echo "Installing a test MySQL instance in $datadir..."
mysql_install_db --no-defaults --user=$user --datadir=$datadir --rpm --force

tmpf=$(mktemp)
# USE mysql;
# UPDATE user SET password=PASSWORD('') WHERE user='root';
# UPDATE user SET plugin='' WHERE user='root';
cat > "$tmpf" <<EOF
FLUSH PRIVILEGES;
ALTER USER 'root'@'localhost' IDENTIFIED BY '';
EOF

$mysqld --bootstrap --skip-grant-tables < "$tmpf"

unlink "$tmpf"

echo "Starting the daemon..."
$mysqld &

pid=$!

echo "Waiting for the server to be actually available..."
c=0;
while ! nc -z 127.0.0.1 $port; do
    c=$(($c+1));
    sleep 3;
    if [ $c -gt 20 ]; then
	echo "Timed out waiting for mysql server to be available" >&2
	if [ "$pid" ]; then
	    kill $pid || :
	    sleep 2
	    kill -s KILL $pid || :
	fi
	exit 1
    fi
done

echo "Checking if the server is running..."
$mysqladmin status

for db in moneta moneta_activerecord1 moneta_activerecord2; do
    echo "Dropping the database $db if it exists..."
    $mysqladmin --force --silent drop $db || true
    echo "Creating new empty database $db..."
    $mysqladmin create $db
done
